// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import vClickOutside from 'v-click-outside'
import axios from "axios";
import store from "./store"

Vue.config.productionTip = false
Vue.use(VueMaterial)

Vue.use(vClickOutside)

// Axios Intercept Requests
axios.interceptors.request.use(async function (config) {
  if (!config.url.includes('public')) {
    config.headers['Authorization'] = "Basic " + localStorage.getItem('auth');
  }
  return config;
}, function (error){
  return Promise.reject(error);
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
