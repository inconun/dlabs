import Vue from 'vue'
import Router from 'vue-router'
import Test from "../components/Test";
import Home from "../views/Home";
import DetailComponent from "../components/DetailComponent";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import ("../views/Home")
    },
    {
      path: '/test',
      name: 'test',
      component: Test
    },
    {
      path:'/deal/:id',
      name: 'deal',
      component: () => import ("../views/DetailView")
    },
    {
      path:'/add',
      name:"addDeal",
      component: () => import("../views/AddDeal.vue")
    }

  ]
})
