package fr.univlittoral.dlabs.persistance.beans;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="tbl_deal")
public class DealDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_deal")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "shop_name")
    private String shopName;

    @Column(name = "shop_link")
    private String shopLink;

    @Column(name = "price_old")
    private long priceOld;

    @Column(name = "price_new")
    private long priceNew;

    @Column(name = "promo_code")
    private String promoCode;

    @Column(name = "date")
    private Date date;

    @Column(name = "img_url")
    private String imgUrl;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "deal")
    private List<TemperatureDO> temperatureDOList;

    @ManyToOne
    @JoinColumn(name="id_user")
    private UserDO user;


    public DealDO() {
    }

// Getters et setters


    public UserDO getUser() {
        return user;
    }

    public void setUser(UserDO user) {
        this.user = user;
    }

    public List<TemperatureDO> getTemperatureDOList() {
        return temperatureDOList;
    }

    public void setTemperatureDOList(List<TemperatureDO> temperatureDOList) {
        this.temperatureDOList = temperatureDOList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public long getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(long priceOld) {
        this.priceOld = priceOld;
    }

    public long getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(long priceNew) {
        this.priceNew = priceNew;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

