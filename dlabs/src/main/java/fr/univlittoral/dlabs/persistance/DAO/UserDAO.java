package fr.univlittoral.dlabs.persistance.DAO;

import fr.univlittoral.dlabs.persistance.beans.UserDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

@Repository
public class UserDAO {


        @Autowired
        private EntityManager entityManager;

        public List<UserDO> getAll(){
            Query q = entityManager.createQuery("from UserDO");
            // Récupération de la liste des résultats
            return  q.getResultList();

        }

        public UserDO getOne(int myId){
            Query q = entityManager.createQuery("from UserDO where id= :id");
            q.setParameter("id",myId);
            return (UserDO) q.getSingleResult();
        }

        @Transactional
        public void insertUser(UserDO myDo){
            entityManager.persist(myDo);
        }



        public UserDO findUserWithName(String name){
            Query q = entityManager.createQuery("from UserDO where pseudo= :pseudo");
            q.setParameter("pseudo",name);
            try{
                return (UserDO) q.getSingleResult();
            }catch(NoResultException e) {
                return null;
            }
        }
    }


