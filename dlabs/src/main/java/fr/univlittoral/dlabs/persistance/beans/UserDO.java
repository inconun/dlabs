package fr.univlittoral.dlabs.persistance.beans;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="tbl_user")
public class UserDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    private Integer id;

    @Column(name = "pseudo")
    private String pseudo;

    @Column(name = "first_name")
    private String first_name;

    @Column(name = "last_name")
    private String last_name;

    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "user")
    private List<TemperatureDO> temperatureDOList;

    @OneToMany(mappedBy = "user")
    private List<DealDO> dealDOList;

    public UserDO() {
    }


    public List<DealDO> getDealDOList() {
        return dealDOList;
    }

    public void setDealDOList(List<DealDO> dealDOList) {
        this.dealDOList = dealDOList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<TemperatureDO> getTemperatureDOList() {
        return temperatureDOList;
    }

    public void setTemperatureDOList(List<TemperatureDO> temperatureDOList) {
        this.temperatureDOList = temperatureDOList;
    }
}
