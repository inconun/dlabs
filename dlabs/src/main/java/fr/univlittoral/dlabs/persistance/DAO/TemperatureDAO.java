package fr.univlittoral.dlabs.persistance.DAO;

import fr.univlittoral.dlabs.persistance.beans.TemperatureDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TemperatureDAO {

        @Autowired
        private EntityManager entityManager;

        public List<TemperatureDO> getAll(){
            Query q = entityManager.createQuery("from TemperatureDO");
            // Récupération de la liste des résultats
            return  q.getResultList();

        }

        public TemperatureDO getOne(int myId){
            Query q = entityManager.createQuery("from TemperatureDO where id= :id");
            q.setParameter("id",myId);
            return (TemperatureDO) q.getSingleResult();

        }
        @Transactional
        public void insertTemperature(TemperatureDO myDo){
            entityManager.persist(myDo);
        }
    }

