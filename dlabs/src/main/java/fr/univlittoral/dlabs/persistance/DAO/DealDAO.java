package fr.univlittoral.dlabs.persistance.DAO;

import fr.univlittoral.dlabs.metier.DealDTO;
import fr.univlittoral.dlabs.metier.DealMapper;
import fr.univlittoral.dlabs.persistance.beans.DealDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class DealDAO {

    @Autowired
    private EntityManager entityManager;

    public List<DealDO> getAll(){
        Query q = entityManager.createQuery("from DealDO");
        // Récupération de la liste des résultats
        return  q.getResultList();

    }

    public DealDO getOne(int myId){
        Query q = entityManager.createQuery("from DealDO where id=:id");
        q.setParameter("id",myId);
        return (DealDO) q.getSingleResult(  );

    }
    @Transactional
    public void insertDeal(DealDO myDo){
        entityManager.persist(myDo);
    }
}
