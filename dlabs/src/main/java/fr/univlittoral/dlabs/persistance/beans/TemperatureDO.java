package fr.univlittoral.dlabs.persistance.beans;

import javax.persistence.*;


@Entity
@Table(name="tbl_temperature")
public class TemperatureDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_temperature")
    private Integer id;

    @Column(name = "value")
    private long value;

    @ManyToOne
    @JoinColumn (name="id_deal")
    private DealDO deal;

    @ManyToOne
    @JoinColumn (name="id_user")
    private UserDO user;

    public TemperatureDO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public DealDO getDeal() {
        return deal;
    }

    public void setDeal(DealDO deal) {
        this.deal = deal;
    }

    public UserDO getUser() {
        return user;
    }

    public void setUser(UserDO user) {
        this.user = user;
    }
}
