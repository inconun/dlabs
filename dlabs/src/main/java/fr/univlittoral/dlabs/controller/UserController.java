package fr.univlittoral.dlabs.controller;

import fr.univlittoral.dlabs.metier.UserBO;
import fr.univlittoral.dlabs.metier.UserDTO;
import fr.univlittoral.dlabs.security.PasswordBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
public class UserController {

    @Autowired
    private UserBO userBO;

    @Autowired
    private PasswordBO passwordBO;

    @RequestMapping(value = "/public/user",method = RequestMethod.POST)
    public void addUser(@RequestBody UserDTO myDTO){
        myDTO.setPassword(passwordBO.encode(myDTO.getPassword()));
        userBO.addUser(myDTO);
    }
}
