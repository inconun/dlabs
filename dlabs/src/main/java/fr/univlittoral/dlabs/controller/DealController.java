package fr.univlittoral.dlabs.controller;

import fr.univlittoral.dlabs.metier.DealBO;
import fr.univlittoral.dlabs.metier.DealDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController

public class DealController {

    @Autowired
    private DealBO dealBO;

    //@CrossOrigin(origins = "http://localhost:8082")//url du serveur front
    @RequestMapping(value = "/public/deals",method = RequestMethod.GET)
    public List<DealDTO> getAll(){
        final List<DealDTO> dealDTOS = dealBO.finalDTO();

        return dealDTOS;
    }
    //@CrossOrigin(origins = "http://localhost:8082")//url du serveur front
    @RequestMapping(method = RequestMethod.GET, value = "/public/deals/{id}")
    public DealDTO getOne(@PathVariable Integer id){
        DealDTO deal = dealBO.getOne(id);

        return deal;
    }

    //@CrossOrigin(origins = "http://localhost:8082")//url du serveur front
    @RequestMapping(value = "/deals",method = RequestMethod.POST)
    public void addDeal(@RequestBody DealDTO myDTO){
        dealBO.addDeal(myDTO);
    }
}
