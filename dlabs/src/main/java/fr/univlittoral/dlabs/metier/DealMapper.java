package fr.univlittoral.dlabs.metier;

import fr.univlittoral.dlabs.persistance.beans.DealDO;
import fr.univlittoral.dlabs.persistance.beans.TemperatureDO;

public class DealMapper {

    private DealMapper() {
    }

    public static DealDTO map(DealDO dealDO) {
        long temperature = 0;
        for (TemperatureDO temperatureDO: dealDO.getTemperatureDOList()) {
            temperature += temperatureDO.getValue();

        }
        DealDTO dealDTO = new DealDTO();
        dealDTO.setAuteur(dealDO.getUser().getPseudo());
        dealDTO.setDate(dealDO.getDate());
        dealDTO.setImgUrl(dealDO.getImgUrl());
        dealDTO.setShopLink(dealDO.getShopLink());
        dealDTO.setShopName(dealDO.getShopName());
        dealDTO.setTemperature(temperature);
        dealDTO.setTitre(dealDO.getTitle());
        dealDTO.setNewPrice(dealDO.getPriceNew());
        dealDTO.setOldPrice(dealDO.getPriceOld());
        dealDTO.setId(dealDO.getId());

        return dealDTO;
    }
/*
    public static DealDO map(DealDTO dealDTO){
        return new DealDO();
    }*/
}
