package fr.univlittoral.dlabs.metier;


import java.util.Date;

public class DealDTO {
    private int id;
    private long temperature;
    private String titre;
    private String imgUrl;
    private String Auteur;
    private String shopName;
    private String shopLink;
    private Date date;
    private long oldPrice;
    private long newPrice;
    private String description;
    private String promoCode;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public long getTemperature() {
        return temperature;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(long oldPrice) {
        this.oldPrice = oldPrice;
    }

    public long getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(long newPrice) {
        this.newPrice = newPrice;
    }

    public void setTemperature(long temperature) {
        this.temperature = temperature;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getAuteur() {
        return Auteur;
    }

    public void setAuteur(String auteur) {
        Auteur = auteur;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "DealDTO{" +
                "id=" + id +
                ", temperature=" + temperature +
                ", titre='" + titre + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", Auteur='" + Auteur + '\'' +
                ", shopName='" + shopName + '\'' +
                ", shopLink='" + shopLink + '\'' +
                ", date=" + date +
                ", oldPrice=" + oldPrice +
                ", newPrice=" + newPrice +
                ", description='" + description + '\'' +
                ", promoCode='" + promoCode + '\'' +
                '}';
    }
}
