package fr.univlittoral.dlabs.metier;

import fr.univlittoral.dlabs.persistance.DAO.UserDAO;
import fr.univlittoral.dlabs.persistance.beans.UserDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserBO {

    @Autowired
    private UserDAO userDAO;

    public void addUser(UserDTO myDto){

        UserDO myDo = new UserDO();
        myDo.setFirst_name(myDto.getFirstName());
        myDo.setLast_name(myDto.getLastName());
        myDo.setPassword(myDto.getPassword());
        myDo.setPseudo((myDto.getPseudo()));

        userDAO.insertUser(myDo);
    }


}
