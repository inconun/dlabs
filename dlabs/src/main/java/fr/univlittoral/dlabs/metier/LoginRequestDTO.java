package fr.univlittoral.dlabs.metier;

public class LoginRequestDTO {
    private String MotDePasse;
    private String Identifiant;

    public String getMotDePasse() {
        return MotDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        MotDePasse = motDePasse;
    }

    public String getIdentifiant() {
        return Identifiant;
    }

    public void setIdentifiant(String identifiant) {
        Identifiant = identifiant;
    }
}
