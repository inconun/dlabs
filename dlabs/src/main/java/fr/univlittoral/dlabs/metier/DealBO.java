package fr.univlittoral.dlabs.metier;

import fr.univlittoral.dlabs.persistance.DAO.DealDAO;
import fr.univlittoral.dlabs.persistance.beans.DealDO;
import fr.univlittoral.dlabs.persistance.beans.UserDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class DealBO {

    @Autowired
    private DealDAO dealDAO;

    public List<DealDTO> finalDTO(){
        final List<DealDO> dealDOS = dealDAO.getAll();

        final List<DealDTO> dealDTOS = new ArrayList<>();
        for (final DealDO d: dealDOS) {
            dealDTOS.add(DealMapper.map(d));
        }

        return dealDTOS;
    }

    public DealDTO getOne(int myId){
        DealDO d = dealDAO.getOne(myId);
        DealDTO dealDTO = DealMapper.map(d);

        dealDTO.setDescription(d.getDescription());
        dealDTO.setPromoCode(d.getPromoCode());
        return dealDTO;
    }

    public void addDeal(DealDTO myDto){
        DealDO myDo = new DealDO();

        // en dur pour pouvoir entrer un deal
        UserDO userDO = new UserDO();
        userDO.setId(1);
        myDo.setUser(userDO);
        /////////

        Date date = new Date();
        myDo.setDate(date);
        myDo.setImgUrl(myDto.getImgUrl());
        myDo.setShopLink(myDto.getShopLink());
        myDo.setShopName(myDto.getShopName());
        myDo.setTitle(myDto.getTitre());
        myDo.setPriceNew(myDto.getNewPrice());
        myDo.setPriceOld(myDto.getOldPrice());
        myDo.setDescription(myDto.getDescription());
        myDo.setPromoCode(myDto.getPromoCode());

        dealDAO.insertDeal(myDo);
    }


}
