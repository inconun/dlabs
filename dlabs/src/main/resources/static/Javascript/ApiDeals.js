function getData(){
    fetch("http://localhost:8080/deals")
        .then(function(response) {
            console.log(response);
            return response.json();
        })
        .then(data => {
            let myDiv = document.querySelector(".card-list");
            for (let element of data){
                myDiv.innerHTML += "<div class=\"card mb-3\"  margin:auto;\" onclick='document.location=\"./detailDeal.html?id=" + element['id'] + "\"'>" +
                    "    <div class=\"row no-gutters\">" +
                    "        <div class=\"col-md-2\">" +
                    "            <img src=\"" + element['imgUrl'] + "\" class=\"card-img\" style='width: 100%; height: 100%;'>" +
                    "        </div>" +
                    "        <div class=\"col-md-7\">" +
                    "            <div class=\"card-body\">" +
                    "                <h5 class=\"card-title\">" + element['temperature'] + "°</h5>" +
                    "                <p class=\"card-text\"> "  + element['newPrice'] + "€ / <span style='text-decoration:line-through; color: grey'>" + element['oldPrice'] + "€</span></p>" +
                    "                <p class=\"card-text\"> "  + element['titre'] + "</p>" +
                    "                <p class=\"card-text\"> "  + element['auteur'] + " | " + element['shopName'] + "</p>" +
                    "            </div>" +
                    "        </div>" +
                    "        <div class=\"col-md-3\">" +
                    "            <div class=\"card-body\">" +
                    "                <p class=\"card-text\">" + element['date'] + "</p>" +
                    "                <p class=\"card-text\"><a href=\"" + element['shopLink'] + "\" class='btn btn-dark'>Voir</a></p>" +
                    "            </div>" +
                    "        </div>" +
                    "    </div>" +
                    "</div>"
            }
        })
        .catch(error => console.log('error', error));
}
