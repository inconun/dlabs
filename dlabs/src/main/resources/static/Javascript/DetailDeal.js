function getDeal(){
    let id = getParam();
    fetch("http://localhost:8080/deals/"+id)
        .then(function(response) {
            return response.json();
        })
        .then(data => {
            let myDiv = document.querySelector(".card-list");
            document.querySelector("#temperature").innerHTML=data['temperature'];
            document.querySelector(".imgUrl").setAttribute("src",data['imgUrl'])
            document.querySelector("#titre").innerHTML=data['titre'];
            document.querySelector("#date").innerHTML=data['date'];
            document.querySelector("#auteur-shopname").innerHTML = data['auteur']+" | "+data['shopName']
            document.querySelector("#shopLink").setAttribute("href",data['shopLink']);
            document.querySelector(".promo_code").innerHTML=data['promoCode'];
            document.querySelector(".description").innerHTML=data['description'];


            document.querySelector(".price").innerHTML=data['newPrice']+ "€ <span style='text-decoration:line-through; color: grey '>" + data['oldPrice'] + "€</span>";

        })
        .catch(error => console.log('error', error));
}


function getParam(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams (queryString);
    return urlParams.get ('id');

}